package quilldelta

import "reflect"

type Delta struct {
	Ops   []Op  `json:"ops,omitempty"`
	Attrs Attrs `json:"attributes,omitempty"`
}

func New() *Delta {
	return &Delta{}
}

func (delta *Delta) Len() int {
	l := 0
	for _, o := range delta.Ops {
		l += Len(o)
	}
	return l
}

func (delta *Delta) Insert(ins interface{}, attrs Attrs) *Delta {
	if str, ok := ins.(string); ok {
		if len(str) == 0 {
			return delta
		}
	}

	op := Op{
		Insert: ins,
		Attrs:  attrs,
	}

	return delta.push(op)
}

func (delta *Delta) Delete(n int) *Delta {
	if n <= 0 {
		return delta
	}

	op := Op{
		Delete: newint(n),
	}
	return delta.push(op)
}

func (delta *Delta) Retain(n int) *Delta {
	return delta
}

func (delta *Delta) push(op Op) *Delta {
	idx := len(delta.Ops)
	lastOpIdx := idx - 1
	if lastOpIdx >= 0 {
		lastOp := delta.Ops[lastOpIdx]

		if op.Delete != nil && lastOp.Delete != nil {
			delta.Ops[lastOpIdx] = Op{
				Delete: newint(*op.Delete + *lastOp.Delete),
			}
			return delta
		}

		if lastOp.Delete != nil && op.Insert != nil {
			idx -= 1
			if len(delta.Ops) == 1 {
				delta.Ops = append([]Op{op}, delta.Ops...)
				return delta
			}
		}

		if reflect.DeepEqual(lastOp.Attrs, op.Attrs) {
			lastStr, lastInsertIsString := lastOp.Insert.(string)
			newStr, newInsertIsString := op.Insert.(string)

			if lastInsertIsString && newInsertIsString {
				delta.Ops[idx-1] = Op{
					Insert: lastStr + newStr,
				}

				if op.Attrs != nil {
					delta.Ops[idx-1].Attrs = op.Attrs
				}

				return delta
			}

			if lastOp.Retain != nil && op.Retain != nil {
				delta.Ops[idx-1] = Op{
					Retain: newint(*lastOp.Retain + *op.Retain),
				}

				if op.Attrs != nil {
					delta.Ops[idx-1].Attrs = op.Attrs
				}

				return delta

			}
		}

		if idx == len(delta.Ops) {
			delta.Ops = append(delta.Ops, op)
		} else {
			delta.Ops = append(delta.Ops[:idx], append([]Op{op}, delta.Ops[idx:]...)...)
		}

		return delta
	}

	delta.Ops = append(delta.Ops, op)
	return delta
}
