package quilldelta

import (
	"reflect"
	"testing"
)

func TestEmptyOpsBuilder(t *testing.T) {
	delta := New().Insert("", nil).Delete(0).Retain(0)

	if len(delta.Ops) != 0 {
		t.Errorf("Should have not ops, but got: %v", delta.Ops)
	}
}

func TestInsertText(t *testing.T) {
	delta := New().Insert("hello", nil)

	if len(delta.Ops) != 1 {
		t.Fatalf("Should have one insert ops, but got: %v", delta.Ops)
	}

	if !reflect.DeepEqual(delta.Ops[0], Op{
		Insert: "hello",
	}) {
		t.Errorf("Should have one insert opt, but got: %v", delta.Ops)
	}
}

func TestInsertWithAttributes(t *testing.T) {
	delta := New().Insert(map[string]interface{}{
		"url": "http://someurl.com",
	}, Attrs{
		"embed": true,
	})

	if len(delta.Ops) != 1 {
		t.Fatalf("Should have one insert ops, but got: %v", delta.Ops)
	}

	if !reflect.DeepEqual(delta.Ops[0], Op{
		Insert: map[string]interface{}{
			"url": "http://someurl.com",
		},
		Attrs: Attrs{
			"embed": true,
		},
	}) {
		t.Errorf("Should have one insert opt, but got: %v", delta.Ops)
	}
}

func TestInsertAfterDelete(t *testing.T) {
	delta := New().Delete(1).Insert("text", nil)
	expected := New().Insert("text", nil).Delete(1)

	if !reflect.DeepEqual(delta, expected) {
		t.Errorf("Deltas should be equal. \nGot: %+v \nExpected: %+v", delta, expected)
	}
}
